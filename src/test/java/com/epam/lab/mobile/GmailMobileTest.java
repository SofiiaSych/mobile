package com.epam.lab.mobile;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.epam.lab.mobile.businessObjects.GmailBO;
import com.epam.lab.mobile.factories.CapabilitiesFactory;
import com.epam.lab.mobile.utils.PropertyParser;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import static com.epam.lab.mobile.factories.CapabilitiesFactory.checkIfNotLoggedIn;
import static com.epam.lab.mobile.factories.CapabilitiesFactory.getCapabilities;

public class GmailMobileTest {
	private final String PROPERTIES_PATH = "src/test/resources/com/epam/lab/mobile/config.properties";

	private final String DEVICE_URL_PROPERTY = "deviceURL";
	private final String IMPLICITLY_WAIT_PROPERTY = "implicitlyWait";
	private final String PAGE_UPDATE_TIMEOUT_PROPERTY = "pageUpdateTimeOut";

	private AndroidDriver<MobileElement> androidDriver;
	private GmailBO gmailBO;

	@BeforeClass
	public void setupDriver() throws MalformedURLException {
		PropertyParser propertyParser = new PropertyParser(PROPERTIES_PATH);
		androidDriver = new AndroidDriver<>(new URL(propertyParser.getProperty(DEVICE_URL_PROPERTY)),
				CapabilitiesFactory.getCapabilities());
		androidDriver.manage().timeouts().implicitlyWait(propertyParser.getIntProperty(IMPLICITLY_WAIT_PROPERTY),
				TimeUnit.SECONDS);
		gmailBO = new GmailBO(androidDriver, propertyParser.getIntProperty(PAGE_UPDATE_TIMEOUT_PROPERTY));
	}

	@Test
	public void loginTest() {
		if (checkIfNotLoggedIn()) {
			gmailBO.skipInitialPage();
			gmailBO.navigateToAccount();
		}
		gmailBO.sendMessage();
	}

	@AfterClass
	public void driverQuit() {
		androidDriver.closeApp();
	}
}
