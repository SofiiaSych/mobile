package com.epam.lab.mobile.businessObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.lab.mobile.pageObjects.GmailAccountsPage;
import com.epam.lab.mobile.pageObjects.GmailComposePage;
import com.epam.lab.mobile.pageObjects.GmailInitialPage;
import com.epam.lab.mobile.pageObjects.GmailMailPage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class GmailBO {
	private static final Logger LOGGER = LogManager.getLogger(GmailBO.class);

	private GmailAccountsPage gmailAccountsPage;
	private GmailComposePage gmailComposePage;
	private GmailInitialPage gmailInitialPage;
	private GmailMailPage gmailMailPage;

	private final String LOG_TAP_MESSAGE = "Tapping %s";
	private final String LOG_FILL_FIELD_MESSAGE = "Filling '%s' field with: %s";
	private final String RECEIVER = "sychsofiiatest2@gmail.com\\n";
	private final String SUBJECT = "my letter";
	private final String MESSAGE = "Some text";

	public GmailBO(AndroidDriver<? extends MobileElement> driver, int pageUpdateTimeOut) {
		gmailAccountsPage = new GmailAccountsPage(driver, pageUpdateTimeOut);
		gmailComposePage = new GmailComposePage(driver, pageUpdateTimeOut);
		gmailInitialPage = new GmailInitialPage(driver, pageUpdateTimeOut);
		gmailMailPage = new GmailMailPage(driver, pageUpdateTimeOut);
	}

	public void skipInitialPage() {
		LOGGER.info(String.format(LOG_TAP_MESSAGE, "OK button on initial page"));
		gmailInitialPage.tapOk();
		LOGGER.info(String.format(LOG_TAP_MESSAGE, "OK button on accounts page"));
		gmailAccountsPage.tapGoToGmail();
	}

	public void navigateToAccount() {
		LOGGER.info(String.format(LOG_TAP_MESSAGE, "login to account"));
		gmailMailPage.login();
	}

	public String sendMessage() {
		LOGGER.info(String.format(LOG_TAP_MESSAGE, "compose button"));
		gmailMailPage.tapComposeButton();
		LOGGER.info(String.format(LOG_FILL_FIELD_MESSAGE, "subject", SUBJECT));
		gmailComposePage.fillSubjectField(SUBJECT);
		LOGGER.info(String.format(LOG_FILL_FIELD_MESSAGE, "message", MESSAGE));
		gmailComposePage.fillMessageField(MESSAGE);
		LOGGER.info(String.format(LOG_FILL_FIELD_MESSAGE, "to", RECEIVER));
		gmailComposePage.fillToField(RECEIVER);
		LOGGER.info(String.format(LOG_TAP_MESSAGE, "send button"));
		gmailComposePage.tapSendButton();
		return gmailComposePage.getCurrentActivity();
	}

}
