package com.epam.lab.mobile.pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class GmailMailPage extends GmailPageObject {
	@AndroidFindBy(className = "android.widget.ImageButton")
	private MobileElement widgetButton;
	@AndroidFindBy(id = "compose_button")
	private MobileElement composeButton;
	@FindBy(className = "android.widget.EditText")
	private WebElement loginInput;
	@FindBy(className = "android.widget.EditText")
	private WebElement passwordInput;
	@FindBy(xpath = "(//android.widget.Button)[3]")
	private WebElement nextButton;
	@FindBy(xpath = "(//android.widget.Button)[4]")
	private WebElement acceptButton;

	public GmailMailPage(AndroidDriver<? extends MobileElement> driver, int pageUpdateTimeOut) {
		super(driver, pageUpdateTimeOut);
	}

	public void login() {
		loginInput.sendKeys("sychsofiiatest@gmail.com\\n");
		String password = "sofiia_test1\\n";
		passwordInput.sendKeys(password);
		acceptButton.click();
	}

	public void tapComposeButton() {
		composeButton.click();
	}
}
